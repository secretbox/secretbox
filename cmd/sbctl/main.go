package main //import "gitlab.com/secretbox/secretbox

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/secretbox/secretbox/sbctl"
)

// Version is the tagged build numbers
var Version string

// CommitHash is the current commit for the build
var CommitHash string

// BuildTime is the build time
var BuildTime string

func main() {
	cmd.Version = Version
	cmd.CommitHash = CommitHash
	cmd.BuildTime = BuildTime

	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp:       true,
		DisableLevelTruncation: true,
	})

	cmd.Execute()
}
