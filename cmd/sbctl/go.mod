module gitlab.com/secretbox/secretbox/cmd/sbctl

replace gitlab.com/secretbox/secretbox => ../../

require (
	github.com/sirupsen/logrus v1.2.0
	gitlab.com/secretbox/secretbox v0.0.0-20180908135253-3d79c1a457fc
)
