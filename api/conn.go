package api

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/secretbox/secretbox/sbctl/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func NewConnection(s *config.Server, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	tls, err := WithTLSorInsecure(s)
	if err != nil {
		return nil, errors.Wrap(err, "could not get authenticated connection to server")
	}
	opts = append(opts, WithToken(s), tls, WithBackoff())
	return grpc.Dial(s.URL, opts...)
}

func WithTLSorInsecure(s *config.Server) (grpc.DialOption, error) {
	switch {
	case len(s.CertFile) > 0:
		tlsCert, err := credentials.NewClientTLSFromFile(s.CertFile, "")
		if err != nil {
			return nil, errors.Wrapf(err, "unable to read cert file at %s", s.CertFile)
		}
		return grpc.WithTransportCredentials(tlsCert), nil
	default:
		return grpc.WithInsecure(), nil
	}
}

func WithBackoff() grpc.DialOption {
	return grpc.WithBackoffMaxDelay(time.Duration(20000))
}

// WithToken adds the authorization header to each outgoing request as a bearer token as long as server.Token is set
func WithToken(s *config.Server) grpc.DialOption {
	return grpc.WithPerRPCCredentials(token{
		token:  s.Token,
		secure: len(s.CertFile) > 0,
	})
}

// WithAuthInterceptor intercepts authorization and OTP errors from the API and automatically prompts for login or OTP
// details to retry the command

// token fulfills the grpc.PerRPCCredentials interface
type token struct {
	token  string
	secure bool
}

func (t token) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	switch {
	case len(t.token) > 0:
		return map[string]string{
			"authorization": "bearer " + t.token,
		}, nil
	default:
		return map[string]string{}, nil
	}
}

func (t token) RequireTransportSecurity() bool {
	return t.secure
}
