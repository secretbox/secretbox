package api

import (
	"errors"

	"github.com/BTBurke/clt"
	"gitlab.com/secretbox/secretbox/config"
	"gitlab.com/secretbox/secretbox/pb"
)

type LoginService interface {
	GetTerminalLogin() (email string, password string)
	GetTerminalChangePassword() (password string, err error)
	Login(email string, password string) (*pb.LoginResponse, error)
	//ChangePassword(password string) (*pb.LoginResponse, error)
	//ResetPassword() (*pb.LoginResponse, error)
}

type loginService struct {
	s *config.Server
	i *clt.InteractiveSession
}

func NewLoginService(s *config.Server) LoginService {
	return &loginService{
		s: s,
		i: clt.NewInteractiveSession(),
	}
}

func (l *loginService) GetTerminalLogin() (email string, password string) {
	email = l.i.Ask("Email: ", clt.Required())
	password = l.i.AskPassword(clt.Required())
	return
}

func (l *loginService) GetTerminalChangePassword() (password string, err error) {
	password = l.i.AskPasswordPrompt("Enter new password: ", clt.Required())
	p2 := l.i.AskPasswordPrompt("Re-enter password: ", clt.Required())
	if p2 != password {
		return "", errors.New("passwords do not match, try `secretbox login` again")
	}
	return password, nil
}

func (l *loginService) Login(email string, password string) (*pb.Response, error) {
	//conn, err :=
	return nil, nil
}
