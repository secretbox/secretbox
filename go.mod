module gitlab.com/secretbox/secretbox

require (
	github.com/BTBurke/clt v1.2.0
	github.com/BTBurke/go-homedir v1.0.0
	github.com/BurntSushi/toml v0.3.1
	github.com/aws/aws-sdk-go-v2 v0.6.0
	github.com/elithrar/simple-scrypt v1.3.0 // indirect
	github.com/golang/protobuf v1.2.0
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793
	golang.org/x/net v0.0.0-20181217023233-e147a9138326
	google.golang.org/grpc v1.17.0
	gopkg.in/src-d/go-git.v4 v4.8.1
)
