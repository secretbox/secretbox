package aws

import (
	"fmt"
	"log"

	"github.com/BTBurke/clt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/pkg/errors"
	"gitlab.com/secretbox/secretbox/config"
)

type AWSProvider struct {
	cfg   aws.Config
	valid bool
}

type CredOption func(c *AWSProvider)

type ConfigProvider func() (*AWSProvider, error)

func NewProvider(opts ...CredOption) (*AWSProvider, error) {
	c := new(AWSProvider)
	for _, opt := range opts {
		opt(c)
		if c.valid {
			break
		}
	}

	if !c.valid {
		return nil, errors.New("unable to load any AWS credential values. try `secretbox setup` first.")
	}
	return c, nil
}

func DefaultCredentials() CredOption {
	return func(c *AWSProvider) {
		cfg, err := external.LoadDefaultAWSConfig()
		if err != nil {
			return
		}
		c.cfg = cfg
		c.valid = true
		return
	}
}

func WithProfile(profile string) CredOption {
	return func(c *AWSProvider) {
		cfg, err := external.LoadDefaultAWSConfig(
			external.WithSharedConfigProfile(profile),
		)
		if err != nil {
			return
		}
		c.cfg = cfg
		c.valid = true
		return
	}
}

func WithCreds(access string, secret string) CredOption {
	return func(c *AWSProvider) {
		cfg, err := external.LoadDefaultAWSConfig(aws.Credentials{
			AccessKeyID:     access,
			SecretAccessKey: secret,
			Source:          "secretbox provided credentials",
		})
		if err != nil {
			return
		}
		c.cfg = cfg
		c.valid = true
		return
	}
}

func Prompt(c *config.Config) CredOption {
	return func(p *AWSProvider) {
		i := clt.NewInteractiveSession()
		fmt.Println("Enter your AWS credentials")
		access := i.Ask("AWS Access Key ID: ")
		if access == "" {
			log.Fatal("Credentials are required to use secretbox")
		}
		secret := i.Ask("AWS Secret Key: ")
		if secret == "" {
			log.Fatal("Credentials are required to use secretbox")
		}

		c.Credentials = append(c.Credentials, config.Credentials{
			Profile: "default",
			Access:  access,
			Secret:  secret,
		})
		if err := c.Write(); err != nil {
			log.Fatal("failed to write credentials to config file")
		}
		cfg, err := external.LoadDefaultAWSConfig(aws.Credentials{
			AccessKeyID:     access,
			SecretAccessKey: secret,
			Source:          "secretbox provided credentials",
		})
		if err != nil {
			return
		}
		p.cfg = cfg
		p.valid = true
		return
	}
}
