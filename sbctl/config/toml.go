package config

import (
	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
	"os"
	"path/filepath"
)

// Writable is a TOML configuration that may be updated and then written to the file.  As long as
// path is defined, the passed in configuration is written to the file returned by Path()
type Writable interface {
	Path() string
}

// Readable is a TOML configuration that can be read from a file.  TOML data is populated in the struct pointer
// returned by Bind and TOML metadata is set via the SetMetadata handler.
type Readable interface {
	Bind() interface{}
	SetMetadata(md toml.MetaData)
}

// WriteTOML is a utility function for path-aware configuration values to write and update
// themselves.
func WriteTOML(cfg Writable, data interface{}) error {
	path := cfg.Path()
	if len(path) == 0 {
		return errors.New("config file path unknown, failed to write config")
	}
	dir := filepath.Dir(path)
	if err := os.MkdirAll(dir, os.ModeDir); err != nil {
		return errors.Wrap(err, "could not create $HOME/.secretbox directory")
	}

	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return errors.Wrap(err, "could not open config file for writing")
	}
	if err := toml.NewEncoder(f).Encode(data); err != nil {
		return errors.Wrap(err, "could not write to config file")
	}
	if err := f.Close(); err != nil {
		return errors.Wrap(err, "could not close config file")
	}
	return nil
}

// ReadTOML will read TOML data and bind it to the struct pointed at by Bind() and set
// metadata about the values that were set
func ReadTOML(path string, cfg Readable) error {
	md, err := toml.DecodeFile(path, cfg.Bind())
	if err != nil {
		return errors.Wrapf(err, "could not read configuration file %s", path)
	}
	cfg.SetMetadata(md)
	return nil
}
