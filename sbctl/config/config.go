package config

import (
	"path/filepath"

	"github.com/BTBurke/go-homedir"
	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

// Provider is an enum type for supported cloud platforms
type Provider string

const (
	// AWS is a supported cloud provider
	AWS Provider = "aws"
)

// T represents the TOML configuration, not all of which may be set.  Prefer Config with contains an embedded T
// with additional metadata about which values were set in the file.
type T struct {
	Email       string
	Org         string
	OrgID       string
	UserAlias   string
	UserID      string
	Server      *Server
	Credentials []Credentials
	Keys        []Key
}

// Server is the API server configuration with optional mutual TLS authentication
type Server struct {
	URL      string
	CertFile string
	Token    string
}

// Credentials represents a set of credentials to access a cloud service via the SDK
type Credentials struct {
	Provider Provider
	Profile  string
	Access   string
	Secret   string
	Region   string
	CredPath string
}

// Key is a customer master key in the could providers KMS.  ID should be sufficient to look up this particular key, with optional
// path to indicate what environment this key would be used in, of the form `secretbox/<environment>/<project>`.  For example, the first
// master key created is `secretbox/ and is used for all projects unless a different key is configured on a per-environment or per-project basis.
type Key struct {
	ID   string
	Path string
}

// Config represents the configuration with additional metadata about which values were set in the file.
type Config struct {
	T
	MetaData toml.MetaData
}

// Path returns the path to the configuration file.  Fulfills the Writable interface so that a call
// to c.Write() will update the configuration in place.
func (c *Config) Path() string {
	path, err := getConfigFileName()
	if err != nil {
		return ""
	}
	return path
}

// Bind returns a pointer to the embedded data struct T to populate TOML data from a file.  Fulfills the Readable
// interface to read TOML.
func (c *Config) Bind() interface{} {
	return &c.T
}

// SetMetadata will set the metadata for a file read.  Fulfills the Readable interface.
func (c *Config) SetMetadata(md toml.MetaData) {
	c.MetaData = md
}

// New writes a minimal configuration to the file
func New(opts ...Option) (*Config, error) {
	cfg := new(Config)

	for _, opt := range opts {
		opt(cfg)
	}

	if err := cfg.Write(); err != nil {
		return nil, errors.Wrap(err, "could not write initial config")
	}
	// read back config to populate metadata
	return Read()
}

// Option is a functional option to populate initial configuration values
type Option func(c *Config)

// WithDefaultAPI sets the API server to the hosted secretbox.io service.  To use an enterprise installation,
// use `secretbox set api <url>`
func WithDefaultAPI() Option {
	return func(c *Config) {
		c.Server = &Server{
			URL: "https://api.secretbox.io",
			//TODO: Add cert file via packr
			CertFile: "",
		}
	}
}

// Read will read the config file located at `%HOME%/.secretbox/config.toml` and return a populated Config
// and metadata bout what values were explicitly set.` If the config file does not exist, the config returned
// is nil.
func Read() (*Config, error) {
	cfgFile, err := getConfigFileName()
	if err != nil {
		log.Fatal("could not find home directory")
	}

	cfg := new(Config)
	if err := ReadTOML(cfgFile, cfg); err != nil {
		return nil, errors.Wrap(err, "failed to read secretbox config file")
	}
	return cfg, nil
}

// MustRead reads the config file located at `%HOME%/.secretbox/config.toml`.  If the config file does not exist, exits
// with an error.
func MustRead() *Config {
	cfg, err := Read()
	if err != nil {
		log.Fatal("Could not read configuration file.  Try running `secretbox login` first.")
	}
	return cfg
}

// Write will update or create the configuration file
func (c *Config) Write() error {
	return WriteTOML(c, c.T)
}

func getConfigFileName() (string, error) {
	p, err := getConfigFilePath()
	if err != nil {
		return "", err
	}
	cfgFile := filepath.Join(p, "config.toml")
	return cfgFile, nil
}

func getConfigFilePath() (string, error) {
	home, err := getHome()
	if err != nil {
		return "", err
	}
	return filepath.Join(home, ".secretbox"), nil
}

func getHome() (string, error) {
	homedir.WinPreferUserProfile = true
	home, err := homedir.Dir()
	if err != nil {
		return "", err
	}
	expand, err := homedir.Expand(home)
	if err != nil {
		return "", err
	}
	return expand, nil
}

// GetHomeDir returns the home directory based on OS type
func GetHomeDir() (string, error) {
	return getHome()
}
