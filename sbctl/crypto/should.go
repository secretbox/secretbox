package crypto

import (
	"strings"
)

// Should determines whether the file currently staged for commit should be encryted before being added to the index
func Should(filename string) bool {
	switch {
	case strings.Contains(filename, ".enc."):
		return true
	case filename == "env.toml":
		return true
	case filename == "values.toml":
		return true
	default:
		return false
	}
}
