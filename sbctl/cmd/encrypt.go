package cmd

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/secretbox/secretbox/config"
	"gitlab.com/secretbox/secretbox/crypto"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

func encryptCmd() *cobra.Command {

	return &cobra.Command{
		Use:   "encrypt",
		Short: "Encrypts objects when called as a helper for the git command",
		Long:  `Encrypts objects when called as a helper for the git command.  You should not call this directly from the command line.  It will be envoked as needed to encrypt files in your git repository when necessary.`,
		Run: func(cmd *cobra.Command, args []string) {
			c := config.MustRead()
			p := getProvider(c)
			if len(args) != 1 {
				log.Fatal("encrypt called without filename")
			}
			filename := args[0]
			log.Printf("secretbox: encrypting %s\n", filename)
			result, err := Encrypt(p, filename, "test", os.Stdin, os.Stdout)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("Result: %v", result)
			os.Exit(0)
		},
	}
}

// EncryptionResult is the result of the encrypt command for each staged file.  For files that should not be
// encrypted, InputHash and OutputHash will be the same and EncryptedKey will be nil.
type EncryptionResult struct {
	InputHash    string
	OutputHash   string
	EncryptedKey []byte
	DidEncrypt   bool
}

// Encrypt is called for each staged file before a commit.  The filename determines whether the file should be
// encrypted with a data encryption key derived from the configured customer master key.  Refer to the docs
// for a list of files that are automatically encrypted, such as env.toml.  Any file can be tagged for encryption
// by adding `.enc.` in the filename (e.g., file.enc.json).  This command is called automatically on every commit.
func Encrypt(c crypto.Encrypter, filename string, keypath string,
	in io.Reader, out io.Writer) (*EncryptionResult, error) {

	fileContent, err := ioutil.ReadAll(in)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read file from stdin")
	}

	var didEncrypt bool
	var encryptedOut []byte
	var dek []byte
	switch should := crypto.Should(filename); should {
	case false:
		// no encrypting required, copy directly to out
		encryptedOut = fileContent
	default:
		keyset, err := c.GenerateDataKey(keypath)
		if err != nil {
			return nil, errors.Wrapf(err, "unable to get customer master key at path %s", keypath)
		}
		encryptedOut, err = c.Encrypt(fileContent, keyset.DEK)
		if err != nil {
			return nil, errors.Wrap(err, "encryption failed")
		}
		didEncrypt = true
		dek = keyset.EncryptedDEK
	}

	inHash := plumbing.ComputeHash(plumbing.BlobObject, fileContent)
	outHash := plumbing.ComputeHash(plumbing.BlobObject, encryptedOut)

	if _, err := out.Write(encryptedOut); err != nil {
		return nil, errors.Wrap(err, "failed to write result to stdout")
	}

	return &EncryptionResult{
		InputHash:    inHash.String(),
		OutputHash:   outHash.String(),
		EncryptedKey: dek,
		DidEncrypt:   didEncrypt,
	}, nil

}
