package cmd

import (
	"context"
	"log"

	"github.com/BTBurke/clt"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/secretbox/secretbox/config"
	"gitlab.com/secretbox/secretbox/pb"
	"google.golang.org/grpc"
)

func loginCmd() *cobra.Command {

	c, err := config.Read()
	if err != nil || c == nil {
		c, err = config.New(config.WithDefaultAPI())
		if err != nil {
			log.Fatalf("unable to create inital configuration: %s", err)
		}
	}

	return &cobra.Command{
		Use:   "login",
		Short: "Login to the secretbox service",
		Long:  "Login to the secretbox service. If you are using the enterprise installation, run `secretbox set api <your api service URL` first.",
		Run: func(cmd *cobra.Command, args []string) {

		},
	}
}

func Login(server *config.Server, email string, password string) (*pb.LoginResponse, error) {
	conn, err := grpc.Dial(server.URL)
	if err != nil {
		return nil, errors.Wrap(err, "could not dial server")
	}
	defer conn.Close()
	client := pb.NewLoginServiceClient(conn)
	resp, err := client.Login(context.Background(), &pb.LoginRequest{
		Email:    email,
		Password: password,
	})
	if err != nil {
		return nil, errors.Wrap(err, "login failed")
	}
	switch resp.Action {
	case pb.LoginAction_SUCCESS:
		return resp, nil
	case pb.LoginAction_CHG_PASSWORD:
		return changePassword(server)
	case pb.LoginAction_OTP_REQUIRED:
		return nil, errors.New("not implemented")
	default:
		return nil, errors.New("unknown response from server")
	}
	return nil, errors.New("test")
}

func changePassword(server *config.Server) (*pb.LoginResponse, error) {
	c := clt.NewInteractiveSession()
	p1 := c.AskPasswordPrompt("Enter password: ", clt.Required())
	p2 := c.AskPasswordPrompt("Re-enter password: ", clt.Required())
	if p1 != p2 {
		return nil, errors.New("passwords do not match, try `secretbox login` again")
	}
	_, err := grpc.Dial(server.URL)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
