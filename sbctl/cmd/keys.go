package cmd

import (
	"os"
	"strings"

	"github.com/BTBurke/clt"

	"github.com/spf13/cobra"
	"gitlab.com/secretbox/secretbox/config"
	"gitlab.com/secretbox/secretbox/crypto"
)

func init() {
	keysCmdRoot.AddCommand(listKeysCmd)
}

func keysCmd() *cobra.Command {
	return keysCmdRoot
}

var keysCmdRoot = &cobra.Command{
	Use:   "keys",
	Short: "Manages customer master keys",
	Long:  `Manages customer master keys in your cloud providers key management system.`,
	Run: func(cmd *cobra.Command, args []string) {
		c := config.MustRead()
		ListKeys(c)
		os.Exit(0)
	},
}

func CreateKey(keypath string, keys crypto.KeyManager) (string, error) {

	// TODO: make API call to save keypath and ID in account
	return keys.CreateMasterKey(keypath)
}

var listKeysCmd = &cobra.Command{
	Use:   "list",
	Short: "List customer master keys",
	Long:  `Lists customer master keys in your cloud providers key management system.`,
	Run: func(cmd *cobra.Command, args []string) {
		c := config.MustRead()
		ListKeys(c)
		os.Exit(0)
	},
}

func ListKeys(c *config.Config) {
	t := clt.NewTable(2)
	t.Title("Secretbox Master Keys")
	t.ColumnHeaders("Project", "Environment")
	for _, key := range c.Keys {
		keySplit := strings.Split(key.Path, "/")
		if keySplit[0] != "secretbox" {
			continue
		}
		switch len(keySplit) {
		case 1:
			t.AddRow("default", "")
		case 2:
			t.AddRow("default", keySplit[1])
		case 3:
			t.AddRow(keySplit[2], keySplit[1])
		default:
			continue
		}
		t.Show()
	}
}
