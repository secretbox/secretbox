package cmd

import (
	"bytes"
	"context"
	"testing"

	"github.com/BTBurke/snapshot"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/secretbox/secretbox/crypto"
)

var DEK = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1}
var EncDEK = []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1}
var EncOut = []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1}

type MockEncrypter struct {
	mock.Mock
}

func (m *MockEncrypter) Encrypt(contents []byte, dataEncryptionKey []byte) ([]byte, error) {
	args := m.Called(contents, dataEncryptionKey)
	return args.Get(0).([]byte), args.Error(1)
}

func (m *MockEncrypter) GenerateDataKey(keyID string) (*crypto.Keyset, error) {
	args := m.Called(keyID)
	return args.Get(0).(*crypto.Keyset), args.Error(1)
}

func (m *MockEncrypter) GenerateDataKeyContext(ctx context.Context, keyID string) (*crypto.Keyset, error) {
	args := m.Called(ctx, keyID)
	return args.Get(0).(*crypto.Keyset), args.Error(1)
}

func TestEncryptCmd(t *testing.T) {
	m := new(MockEncrypter)
	m.On("Encrypt", []byte("test input"), DEK).Return(EncOut, nil)
	m.On("GenerateDataKey", "test").Return(&crypto.Keyset{
		DEK:          DEK,
		EncryptedDEK: EncDEK,
	}, nil)

	in := bytes.NewBufferString("test input")
	out := new(bytes.Buffer)

	result, err := Encrypt(m, "test.enc.txt", "test", in, out)
	assert.Equal(t, EncOut, out.Bytes())
	snapshot.Assert(t, result)
	assert.Nil(t, err)
	m.AssertExpectations(t)

}
