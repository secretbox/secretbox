package git

import (
	"io/ioutil"
	"os"
	"path"

	"github.com/gobuffalo/packr"
	"github.com/pkg/errors"
	git "gopkg.in/src-d/go-git.v4"
)

func Init(repoPath string) error {
	box := packr.NewBox("../bundle")

	var files = []struct {
		TemplateFile string
		File         string
	}{
		{"gitattributes.txt", ".gitattributes"},
		{"env.starter.toml", "env.toml"},
		// TODO: Add readme with additional starter info
	}
	repo, err := git.PlainInit(repoPath, false)
	if err != nil {
		return errors.Wrap(err, "failed to initialize git repo")
	}
	if err := os.Mkdir(path.Join(repoPath, ".secretbox"), os.ModeDir|0700); err != nil {
		return errors.Wrap(err, "failed to create .secretbox directory")
	}
	workTree, err := repo.Worktree()
	if err != nil {
		return errors.Wrap(err, "failed to get worktree for git repo")
	}
	if _, err := workTree.Add(path.Join(repoPath, ".secretbox")); err != nil {
		return errors.Wrap(err, "failed to add .secretbox to git index")
	}
	// Add starter files
	for _, f := range files {
		data, err := box.MustBytes(f.TemplateFile)
		if err != nil {
			return errors.Wrapf(err, "failed to get %s template", f.TemplateFile)
		}
		if err := writeAndCommit(repoPath, data, f.File, workTree); err != nil {
			return errors.Wrapf(err, "failed to write file %s", f.File)
		}
	}
	return nil
}

func writeAndCommit(repoPath string, data []byte, file string, worktree *git.Worktree) error {
	if err := ioutil.WriteFile(path.Join(repoPath, file), data, 0644); err != nil {
		return err
	}
	if err := worktree.AddGlob(file); err != nil {
		return err
	}
	return nil
}
