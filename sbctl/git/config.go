package git

import (
	"fmt"
	"strings"

	"github.com/emirpasic/gods/sets/treeset"
	"github.com/pkg/errors"
)

type Config struct {
	Keypath string
	Project string
	Files   []Object

	// fileSet keeps one set of files on each pass of git commit
	// On each add, the entire fileSet is copied to Files
	fileSet *treeset.Set
	path    string
}

func byBranchPath(a, b interface{}) int {
	c1, ok := a.(Object)
	if !ok {
		return 0
	}
	c2, ok := b.(Object)
	if !ok {
		return 0
	}
	c1Path := strings.Join([]string{c1.Branch, c1.Path}, "/")
	c2Path := strings.Join([]string{c2.Branch, c2.Path}, "/")
	return strings.Compare(c1Path, c2Path)
}

func NewConfig(path string, opts ...ConfigOption) *Config {
	cfg := new(Config)
	tree := treeset.NewWith(byBranchPath)
	cfg.fileSet = tree
	for _, opt := range opts {
		opt(cfg)
	}
	cfg.path = path
	return cfg
}

// AddFile is called as files are processed on each commit.  They are initially added to a set
// and copied to a slice for reading/writing to file
func (c *Config) AddFile(obj Object) {
	c.fileSet.Add(obj)
	files := c.fileSet.Values()
	c.Files = []Object{}
	for _, f := range files {
		a := f.(Object)
		c.Files = append(c.Files, a)
	}
}

func (c *Config) Validate() error {

	var errs []string
	switch {
	case len(c.Keypath) == 0:
		errs = append(errs, "Keypath not defined")
	case len(c.Project) == 0:
		errs = append(errs, "Project path not defined")
	default:
	}
	if len(errs) > 0 {
		return errors.New(fmt.Sprintf("git configuration error: %s", strings.Join(errs, ";")))
	}
	return nil
}

func (c *Config) Write() error {
	return nil
}

type ConfigOption func(c *Config)

func WithKeypath(keyPath string) ConfigOption {
	return func(c *Config) {
		c.Keypath = keyPath
	}
}

func WithProject(proj string) ConfigOption {
	return func(c *Config) {
		c.Project = proj
	}
}
