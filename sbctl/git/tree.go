package git

import (
	"io"
	"strings"

	"github.com/emirpasic/gods/sets/treeset"
	"github.com/pkg/errors"
	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/filemode"
)

// Object represents a file blob in the git index
type Object struct {
	Branch  string
	Path    string
	Hash    string
	EncHash string
}

func byObjPath(a, b interface{}) int {
	c1, ok := a.(Object)
	if !ok {
		return 0
	}
	c2, ok := b.(Object)
	if !ok {
		return 0
	}

	c1Path := strings.Join([]string{c1.Path, c1.Hash}, "/")
	c2Path := strings.Join([]string{c2.Path, c2.Hash}, "/")

	return strings.Compare(c1Path, c2Path)
}

// CreateRepoTree returns the set of all files referenced in the git index with branch
// and SHA1 hash
func CreateRepoTree(path string) (*treeset.Set, error) {

	repo, err := git.PlainOpenWithOptions(path, &git.PlainOpenOptions{
		DetectDotGit: true,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not find valid git repository")
	}

	branches, err := repo.Branches()
	if err != nil {
		return nil, errors.Wrap(err, "failed to get branches")
	}

	set := treeset.NewWith(byObjPath)

	for {
		ref, err := branches.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, "failed to get reference to branch")
		}

		workTree, err := repo.Worktree()
		if err != nil {
			return nil, errors.Wrap(err, "failed to get worktree from git index")
		}
		if err := workTree.Checkout(&git.CheckoutOptions{
			Branch: ref.Name(),
		}); err != nil {
			return nil, errors.Wrapf(err, "failed to checkout branch %s - please commit or stash any unsaved work", ref.Name().String())
		}

		index, err := repo.Storer.Index()
		if err != nil {
			return nil, errors.Wrap(err, "could not get index")
		}

		for _, entry := range index.Entries {
			switch entry.Mode {
			case filemode.Regular:
				set.Add(
					Object{
						Branch: ref.Name().String(),
						Path:   entry.Name,
						Hash:   entry.Hash.String(),
					},
				)
			default:
			}
		}
	}
	return set, nil
}
