package git

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"testing"
	"time"

	"github.com/BTBurke/snapshot"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

func mkTestRepo() (string, error) {
	repoDir, err := ioutil.TempDir("", "secretbox")
	if err != nil {
		return "", err
	}

	createFile := func(dir string, number int, content string) error {
		return ioutil.WriteFile(path.Join(dir, fmt.Sprintf("tmp%d.txt", number)), []byte(content), 0644)
	}

	// create a nested tree of test files
	for i := 0; i < 2; i++ {
		if err := createFile(repoDir, i, fmt.Sprintf("test file %d", i)); err != nil {
			return "", err
		}
	}
	if err := os.Mkdir(path.Join(repoDir, "a"), os.ModeDir|0700); err != nil {
		return "", err
	}
	for i := 0; i < 4; i++ {
		if err := createFile(path.Join(repoDir, "a"), i, fmt.Sprintf("test file a/%d", i)); err != nil {
			return "", err
		}
	}

	// create master branch with nested folder structure
	repo, err := git.PlainInit(repoDir, false)
	if err != nil {
		return "", err
	}
	workTree, err := repo.Worktree()
	if err != nil {
		return "", err
	}
	if err := workTree.AddGlob("*.txt"); err != nil {
		return "", err
	}
	if err := workTree.AddGlob("a/"); err != nil {
		return "", err
	}
	if _, err := workTree.Commit("testing commit", &git.CommitOptions{
		All: true,
		Author: &object.Signature{
			Name:  "Bryan Burke",
			Email: "bryan@secretbox.io",
			When:  time.Now(),
		},
		Committer: &object.Signature{
			Name:  "Bryan Burke",
			Email: "bryan@secretbox.io",
			When:  time.Now(),
		},
	}); err != nil {
		return "", err
	}

	//create development branch with additional file
	if err := workTree.Checkout(&git.CheckoutOptions{
		Branch: plumbing.ReferenceName("refs/heads/development"),
		Create: true,
	}); err != nil {
		return "", errors.Wrap(err, "failed to create development branch")
	}
	for i := 10; i < 12; i++ {
		if err := createFile(repoDir, i, fmt.Sprintf("test file dev %d", i)); err != nil {
			return "", err
		}
	}
	if err := workTree.AddGlob("*.txt"); err != nil {
		return "", err
	}
	if err := workTree.AddGlob("a/"); err != nil {
		return "", err
	}
	if _, err := workTree.Commit("testing dev commit", &git.CommitOptions{
		All: true,
		Author: &object.Signature{
			Name:  "Bryan Burke",
			Email: "bryan@secretbox.io",
			When:  time.Now(),
		},
		Committer: &object.Signature{
			Name:  "Bryan Burke",
			Email: "bryan@secretbox.io",
			When:  time.Now(),
		},
	}); err != nil {
		return "", err
	}

	return repoDir, nil
}

func TestTree(t *testing.T) {
	repoDir, err := mkTestRepo()
	//defer os.RemoveAll(repoDir)
	if err != nil {
		t.Fatalf("error creating test repo: %s", err)
	}
	tree, err := CreateRepoTree(repoDir)
	assert.Nil(t, err)
	if tree != nil {
		snapshot.Assert(t, []byte(tree.String()))
	}
}
